# opsworks_force_https cookbook

Simple cookbook to modify ```/etc/nginx/sites-available/#{application}``` file to force all HTTP traffics to HTTPS in Amazon OpsWorks using nginx and ELB.


# Usage

If you're using a _Rails App Server_ layer type you'll need to add the following custom recipes to your layer:

**Setup**

opsworks_force_https::setup

# Attributes

No expected attributes.


# Recipes

**opsworks_force_https::setup** - setup force https
