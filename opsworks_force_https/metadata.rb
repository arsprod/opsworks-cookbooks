name             'opsworks_force_https'
description      'Changes nginx config to force all HTTP traffics to HTTPS.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

%w[ubuntu debian].each do |os|
  supports os
end
