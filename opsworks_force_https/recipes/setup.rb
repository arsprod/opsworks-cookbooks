#
# cookbook name:: opsworks_force_https
# recipe:: setup
#

node[:deploy].each do |application, deploy|

  service "nginx" do
    supports :status => true
    action :nothing
  end

  # Add HTTP => HTTPS redirect
  template "/tmp/http_redirect.conf" do
    source "setup/http_redirect.conf.erb"
  end

  execute "redirect HTTP to HTTPS" do
    cwd "/etc/nginx/sites-available"
    command "cat #{application} /tmp/http_redirect.conf > /tmp/#{application}.conf && cat /tmp/#{application}.conf > #{application}"
    notifies :restart, "service[nginx]", :immediately
  end
end
